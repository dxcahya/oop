<?php 

    require_once('animals.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animals("shaun");

    echo "Name : " . $sheep->name . "<br>"; 
    echo "Legs : " . $sheep->legs . "<br>"; // 2
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // false


    $sungokong = new Ape("kera sakti");
    

    echo "<br> Name : " . $sungokong->name . "<br>"; 
    echo "Legs : " . $sungokong->legs . "<br>"; 
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; 
    $sungokong->yell(); // "Auooo"


    $kodok = new Frog("buduk");

    echo "Name : " . $kodok->name . "<br>"; 
    echo "Legs : " . $kodok->legs . "<br>"; 
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
    $kodok->jump() ; 



?>